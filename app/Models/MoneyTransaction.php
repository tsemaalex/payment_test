<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoneyTransaction extends Model
{
    /**
     * @var string
     */
    protected $table = 'money_transactions';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'amount',
        'success',
        'payment_system_public_id',
    ];

    /**
     * Relation with user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
