<?php

namespace App\Http\Controllers;

use App\Http\Requests\BalanceDepositRequest;
use App\Models\MoneyTransaction;
use App\Services\BalanceService;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    protected $balance_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->balance_service = new BalanceService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('balance');
    }

    /**
     * Add balance
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(BalanceDepositRequest $request)
    {
        $charge = $this->balance_service->deposit($request);

//        dd($charge);

        return redirect()->route('balance');
    }
}
