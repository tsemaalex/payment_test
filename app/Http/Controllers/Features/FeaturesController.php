<?php

namespace App\Http\Controllers\Features;

use App\Http\Requests\FeatureSubscribeRequest;
use App\Services\FeatureService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FeaturesController extends Controller
{
    protected $feature_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->feature_service = new FeatureService();
    }

    /**
     * Show the application dashboard.
     *
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($name)
    {
        $feature = $this->feature_service->show($name);

        if (!$feature) {
            return redirect()->route('home');
        }

        return view('features.feature', [
            'feature' => $feature,
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buy(Request $request)
    {
        $feature = $this->feature_service->show($request->feature_name);

        return view('features.buy', [
            'feature' => $feature,
        ]);
    }

    /**
     * @param FeatureSubscribeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribe(FeatureSubscribeRequest $request)
    {
        $feature = $this->feature_service->show($request->featureName);

        $this->feature_service->subscribeRequest($request);

        return redirect()->route('feature', $feature->name);
    }
}
