<?php

namespace App\Http\Middleware;

use App\Models\Feature;
use App\Models\User;
use App\Services\FeatureService;
use Closure;

class CheckFeatureSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $feature_name = $request->route('name');
        $feature = Feature::where('name', $feature_name)->firstOrFail();

        if (FeatureService::checkSign($feature, $request->user())) {
            return $next($request);
        }

        if (FeatureService::enoughMoney($feature->price, $request->user())) {
            return redirect()->route('feature.buy.index', [
                'feature_name' => $feature_name
            ]);
        }
        
        return redirect()->route('balance');
    }
}
