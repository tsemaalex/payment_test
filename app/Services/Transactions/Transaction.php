<?php


namespace App\Services\Transactions;


use App\Models\User;

abstract class Transaction
{
    public $amount;
    public $currency;
    public $user;
    public $token;
    public $payment_system_public_id;

    public function __construct($amount, User $user, $currency = null, $token = null, $payment_system_public_id = null)
    {
        if (!$currency) {
            $currency = env('STRIPE_CURRENCY', 'USD');
        }

        $this->amount = $amount;
        $this->currency = $currency;
        $this->user = $user;
        $this->token = $token;
        $this->payment_system_public_id = $payment_system_public_id;
    }
}