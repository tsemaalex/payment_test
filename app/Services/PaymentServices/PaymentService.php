<?php


namespace App\Services\PaymentServices;


abstract class PaymentService
{
//    protected $charge_id;

    /**
     * @param $amount
     * @return mixed
     */
    abstract function charge($amount);
}