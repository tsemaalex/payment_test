<?php


namespace App\Services\PaymentServices;


use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Psy\Util\Str;

class StripeServiceAdapter extends PaymentService
{
    /*protected $token;
    protected $charge_id;

    public function __construct($token)
    {

    }*/

    /**
     * @param $amount
     * @param null $token
     * @return mixed
     */
    public function charge($amount, $token = null)
    {
        $currency = env('STRIPE_CURRENCY', 'USD');

        $charge = Stripe::charges()->create(
            [
                'source'   => $token,
                'currency' => $currency,
                'amount'   => $amount
            ]
        );

        if (!$charge) {
            //todo return error
            return null;
        }

        //todo запись в базу

//        return $charge['id'];
        return $charge;
    }

}