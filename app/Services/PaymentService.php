<?php


namespace App\Services;


use App\Models\MoneyTransaction;
use App\Services\PaymentServices\StripeServiceAdapter;
use App\Services\Transactions\Credit;
use App\Services\Transactions\Debit;
use Illuminate\Http\Request;

class PaymentService
{
    protected $payment_service;

    public function __construct()
    {
        $this->payment_service = new StripeServiceAdapter();
    }

    public function charge(Request $request)
    {
        //todo if there is no $request->user()

        $amount = env('PAYMENT_AMOUNT', 100);

        $charge = $this->payment_service->charge($amount, $request->stripeToken);

        /*if ($charge) {
            $transaction = new MoneyTransaction();

            $transaction->amount = $amount;
            $transaction->success = true;
            $transaction->info = '';
//            $transaction->token = $token;
            $transaction->payment_system_public_id = $charge['id'];

            $request->user()->transactions()->save($transaction);

            return $charge;
        }*/

        return $charge;
    }

    /**
     * @param Credit|Debit $transaction
     * @return bool
     */
    public static function store($transaction)
    {
        $transaction_db = new MoneyTransaction();

        $transaction_db->user_id = $transaction->user->id;
        $transaction_db->amount = $transaction->amount;
        $transaction_db->success = true; //todo from transaction too
        $transaction_db->info = '';
        $transaction_db->token = null;
        $transaction_db->payment_system_public_id = $transaction->payment_system_public_id;

        return $transaction_db->save();
    }
}