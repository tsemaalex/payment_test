<?php


namespace App\Services;


use App\Http\Requests\FeatureSubscribeRequest;
use App\Models\Feature;
use App\Models\User;
use App\Services\Transactions\Credit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeatureService
{
    protected $balance_service;

    public function __construct()
    {
        $this->balance_service = new BalanceService();
    }

    /**
     * Get all features.
     *
     * @return Feature[]|\Illuminate\Database\Eloquent\Collection
     */
    public function showAll()
    {
        return Feature::all();
    }

    /**
     * Get one feature.
     *
     * @param string $name
     * @return mixed
     */
    public function show(string $name)
    {
        return Feature::where('name', $name)->first();
    }


    /**
     * Check if user signed for feature
     *
     * @param Feature $feature
     * @param User $user
     * @return mixed
     */
    public static function checkSign(Feature $feature, User $user)
    {
        return $user->features()->whereName($feature->name)->exists();
    }

    /**
     * Check if user have enough money.
     *
     * @param int|float $amount
     * @param User $user
     * @return bool
     */
    public static function enoughMoney($amount, User $user)
    {
        return $user->balance >= $amount;
    }

    /**
     * Subscribe user to feature request.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function subscribeRequest(FeatureSubscribeRequest $request)
    {
        $feature = $this->show($request->featureName);

        return $this->subscribe($feature, $request->user());
    }

    /**
     * Subscribe user to feature.
     *
     * @param Feature $feature
     * @param User $user
     * @return mixed
     */
    public function subscribe(Feature $feature, User $user)
    {
        DB::transaction(function () use ($user, $feature) {
            $credit = new Credit($feature->price, $user);
            PaymentService::store($credit);

            $this->balance_service->change($credit);
            $user->features()->attach($feature->id);
        });

        return $feature;
    }
}