<?php


namespace App\Services;


use App\Http\Requests\BalanceDepositRequest;
use App\Models\User;
use App\Services\Transactions\Credit;
use App\Services\Transactions\Debit;
use Illuminate\Support\Facades\DB;

class BalanceService
{
    protected $payment_service;

    public function __construct()
    {
        $this->payment_service = new PaymentService();
    }

    /**
     * Get user balance
     *
     * @param User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->balance;
    }

    /**
     * Change user balance
     *
     * @param Debit|Credit $transaction
     * @return bool
     * @throws \Exception
     */
    public function change($transaction)
    {
        if ($transaction instanceof Debit) {
            $newBalance = bcadd($transaction->user->balance, $transaction->amount, 2);
        } else if ($transaction instanceof Credit) {
            $newBalance = bcsub($transaction->user->balance, $transaction->amount, 2);
        } else {
            throw new \Exception('Not a Debit or Credit class');
        }

        $transaction->user->balance = $newBalance;

        return $transaction->user->save();
    }

//    /**
//     * Increase user balance
//     *
//     * @param Debit|Credit $transaction
//     * @return bool
//     */
//    public function increase($transaction)
//    {
//        return $this->change($transaction);
//    }
//
//    /**
//     * Decrease user balance
//     *
//     * @param Debit|Credit $transaction
//     * @return bool
//     */
//    public function decrease($transaction)
//    {
//        return $this->change($transaction);
//    }

    /**
     * Deposit balance
     *
     * @param BalanceDepositRequest $request
     * @return bool
     * @throws \Exception
     */
    public function deposit(BalanceDepositRequest $request)
    {
//        DB::transaction(function () use ($request) {
        $charge = $this->payment_service->charge($request);

        if ($charge) {
            $debit = new Debit(env('PAYMENT_AMOUNT', 100), $request->user(), null, null, $charge['id']);

            PaymentService::store($debit);
            $this->change($debit);
        }
//        });

        return $charge;
    }
}