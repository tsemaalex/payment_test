@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach($errors->all() as $message)
                                <div class="alert alert-warning" role="alert">
                                    {{ $message }}
                                </div>
                            @endforeach
                        @endif

                        @if($feature)
                            Do you really want to buy {{ $feature->name }}?
                            <br>
                            Your balance: {{ Auth::user()->balance }}
                            <br>
                            Feature price: {{ $feature->price }}

                            <form action="{{ route('feature.subscribe')}}"
                                  method="post">
                                @csrf
                                <input type="hidden" name="featureName" value="{{ $feature->name }}">
                                <input type="submit" name="submit">
                            </form>

                        @else
                            There is nothing to buy. What a hell are you doing here?
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection