@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Here is your balance! {{ Auth::user()->balance }}
                        <form action="{{ route('balance.add') }}" method="POST">
                            @csrf
                            <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="{{ env('STRIPE_KEY') }}"
                                    data-amount="{{ env('PAYMENT_AMOUNT', 100) * 100 }}"
                                    data-name="Demo Site"
                                    data-description="Example charge"
                                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                    data-locale="auto">
                            </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
