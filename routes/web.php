<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/balance', 'BalanceController@index')->name('balance');

Route::get('/feature/{name}', 'Features\FeaturesController@index')->name('feature')
    ->middleware('feature.subscription');

Route::get('buy_feature', 'Features\FeaturesController@buy')->name('feature.buy.index');

Route::post('buy_feature', 'Features\FeaturesController@subscribe')->name('feature.subscribe');
Route::post('add_balance', 'BalanceController@add')->name('balance.add');
