<?php
/**
 * Created by PhpStorm.
 * User: madaolex
 * Date: 25.03.19
 * Time: 16:22
 */

use App\Models\Feature;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Feature::class, function (Faker $faker) {
    return [
        'name'  => $faker->unique()->word,
        'price' => $faker->numberBetween(0, 500),
    ];
});