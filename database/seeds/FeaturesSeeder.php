<?php

use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Remove old
        DB::table('features')->delete();

        factory(\App\Models\Feature::class, 10)->create();
    }
}
